export function setup(ctx) {
	// debug
	const debugLog = (...msg) => {
		mod.api.SEMI.log(`${id} v10575`, ...msg);
	};

	// variables
	const id = "semi-auto-bury";
	const name = "SEMI Auto Bury";
	const icon = ctx.getResourceUrl('semi_icon.png');

	const itemList = game.items.filter(item => item instanceof BoneItem || item instanceof SoulItem)
			.sort((a, b) => ((a.sellsFor.currency.uid * 1000) + a.sellsFor.currency.quantity) - ((b.sellsFor.currency.uid * 1000) + b.sellsFor.currency.quantity));

	let config = {
		items: [],
		enabled: false,
		minItemCount: 1,
		notification: false,
	};

	const isItemSelected = (itemid) => config.items.indexOf(itemid) !== -1;
	const isProtectLockedItem = (item) => (config.protectLockedItems && !game.bank.hasUnlockedItem(item));
	const toggleItem = (itemid) => {
		let idx = config.items.indexOf(itemid);
		if (idx !== -1) {
			config.items.splice(idx, 1);
		}
		else {
			config.items.push(itemid);
		}
	};

	// modal
	const injectModal = () => {
		// Overlay Modal
		const scriptModal = mod.api.SEMI.buildModal(id, `${name}`);
		scriptModal.blockContainer.html(`
			<div class="block-content pt-0 font-size-sm">
				<div class="row semi-grid xs pt-1 mb-4">
					<div class="col-md-5 col-sm-12">
						<div class="w-100 mb-3">
							<div class="custom-control custom-switch custom-control-lg">
								<input class="custom-control-input" type="checkbox" name="${id}-enable-check" id="${id}-enable-check">
								<label class="font-weight-normal ml-2 custom-control-label" for="${id}-enable-check">Enable ${name}</label>
							</div>
						</div>
						<div class="w-100">
							<div class="custom-control custom-switch custom-control-sm">
								<input class="custom-control-input" type="checkbox" name="${id}-protect-items-enable-check" id="${id}-protect-items-enable-check">
								<label class="font-weight-normal ml-2 custom-control-label" for="${id}-protect-items-enable-check">Ignore Locked Bank Items</label>
							</div>
						</div>
						<div class="w-100">
							<div class="custom-control custom-switch custom-control-sm">
								<input class="custom-control-input" type="checkbox" name="${id}-show-notifications-enable-check" id="${id}-show-notifications-enable-check">
								<label class="font-weight-normal ml-2 custom-control-label" for="${id}-show-notifications-enable-check">Show Bury Notifications</label>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="w-100 pl-1">Minimum quantity to keep:</div>
						<div class="w-50"><input type="number" min="0" class="form-control m-1" name="${id}-min-item-keep" id="${id}-min-item-keep" placeholder="1"></div>
					</div>
					<div class="col-md-3 col-sm-12">
						<div class="w-100 semi-grid-item mb-1">
							Disabled
						</div>
						<div class="w-100 semi-grid-item enable">
							Enabled
						</div>
					</div>
				</div>
			</div>
            <div class="block-content pt-0 font-size-sm">
                <div class="pb-4">
                    <h2 class="content-heading border-bottom mb-2 pb-2">Item Toggles</h2>
                    <div class="row semi-grid"  id="${id}-container">

                    </div>
                </div>
            </div>
        `);

		$(scriptModal.modal).on('hidden.bs.modal', () => {
			clearContainer();
			storeConfig();

			if (config.enabled) {
				itemList.forEach(item => {
					processItem(item);
				});
			}
		});

		$(`#${id}-enable-check`).on('change', function (e) {
			toggleEnabledStatus();
		}).prop('checked', config.enabled);

		$(`#${id}-protect-items-enable-check`).on('change', function (e) {
			toggleSellLockItemStatus();
		}).prop('checked', config.protectLockedItems);

		$(`#${id}-show-notifications-enable-check`).on('change', function (e) {
			toggleNotificationStatus();
		}).prop('checked', config.notification);

		$(`#${id}-min-item-keep`).bind('keyup change input', function (e) {
			let newMin = parseInt($(this).val(), 10);
			if (isNaN(newMin) || newMin < 0) newMin = 0;
			config.minItemCount = newMin;
		}).val(config.minItemCount);
	};

	const showModal = () => {
		updateModal();
		$(`#modal-${id}`).modal('show');
	};

	const updateModal = () => {
		clearContainer();
		buildItems();
	};

	const clearContainer = () => {
		$(`#${id}-container`).empty();
	};

	const toggleEnabledStatus = () => {
		config.enabled = !config.enabled;
	};

	const toggleSellLockItemStatus = () => {
		config.protectLockedItems = !config.protectLockedItems;
	};

	const toggleNotificationStatus = () => {
		config.notification = !config.notification;
	};

	const buildItems = () => {
		$(`#${id}-container`).html(itemList.map(buildItem).join(''));

		$(`#${id}-container .semi-grid-item`).on('click', function (e) {
			const elm = $(this);
			const itemid = elm.data("item");

			elm.toggleClass('enable');
			toggleItem(itemid);
		});
	}

	const buildItem = (item) => {
		return `<div class="p-1 col-lg-4 col-md-6 col-sm-12">
        <div class="semi-grid-item ${isItemSelected(item.id) ? 'enable' : ''}" data-item="${item.id}">
            <img src="${item.media}" alt="${item.name}" /> ${item.name}
        </div>
    </div>`;
	}

	// script
	const buryItem = (item, quantity) => {
		game.bank.removeItemQuantity(item, quantity, true);
		game.stats.Prayer.add(PrayerStats.BonesBuried, quantity);
		game.stats.Items.add(item, ItemStats.TimesBuried, quantity);

		const pointsToGive = game.bank.getPrayerPointsPerBone(item) * quantity;
		game.combat.player.addPrayerPoints(pointsToGive);
	};

	const releaseSoulItem = (item, quantity) => {
		game.bank.removeItemQuantity(item, quantity, true);
		game.stats.Prayer.add(PrayerStats.SoulsReleased, quantity);
		game.stats.Items.add(item, ItemStats.TimesReleased, quantity);

		const pointsToGive = game.bank.getSoulPointsPerSoul(item) * quantity;
		game.combat.player.addSoulPoints(pointsToGive);
	};

	const processItem = (item) => {
		if (!config.enabled || !isItemSelected(item.id) || isProtectLockedItem(item) || !game.prayer.isUnlocked) {
			return;
		}

		const qty = game.bank.getQty(item) - config.minItemCount;

		// Exit if there's nothing to open
		if (qty <= 0) {
			return;
		}

		// bury the items
		if(item instanceof BoneItem)
			buryItem(item, qty);
		else if (item instanceof SoulItem)
			releaseSoulItem(item, qty);

		// Send a notification to the user
		debugLog(`Buried ${numberWithCommas(qty)} x ${item.name}`);

		if (config.notification)
			game.notifications.createInfoNotification(`semi-auto-bury-${item.id}`, `Buried ${numberWithCommas(qty)} x ${item.name}`, icon, 0);
	}

	// config
	const storeConfig = () => {
		ctx.characterStorage.setItem('config', config);
	}

	const loadConfig = () => {
		const storedConfig = ctx.characterStorage.getItem('config');
		if (!storedConfig) {
			return;
		}

		config = { ...config, ...storedConfig };
	}

	// hooks + game patches
	ctx.onCharacterLoaded(ctx => {
		loadConfig();

		// First do a pass on the players bank, opening any chests that have accumulated before the mod ran
		if (config.enabled) {
			itemList.forEach(item => {
				processItem(item);
			});
		}

		// Patches the add item to bank function, running this every time an object we care about is added to the bank
		ctx.patch(Bank, 'addItem').after((didAddItem, item, ...args) => {
			if (didAddItem) {
				processItem(item);
			}
		});
	});

	ctx.onInterfaceReady(() => {
		injectModal();
		mod.api.SEMI.addSideBarModSetting(name, icon, showModal);
	});
}
